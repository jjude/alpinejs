ref: https://ryangjchandler.co.uk/posts/build-a-remaining-character-count-component-with-alpinejs
tweet btn: https://codepen.io/adamjohnson/pen/xxROJjX?editors=1111


x-ref in combination with $refs is a useful utility for easily accessing DOM elements directly. It's most useful as a replacement for APIs like getElementById and querySelector.